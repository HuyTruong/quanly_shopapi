const ListProduct = [];
const getEle = function (id) {
    return document.getElementById(id);
}

const getListProduct = function () {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "GET",
    })
        .then(function (res) {
            console.log(res);
            let listproduct = mapData(res.data);
            createListProduct(listproduct);

        })
        .catch(function (error) {
            console.log(error);
        })
}

const mapData = function (dataFromDB) {
    for (let i = 0; i < dataFromDB.length; i++) {
        let mappedProduct = new Product(
            dataFromDB[i].id,
            dataFromDB[i].name,
            dataFromDB[i].price,
            dataFromDB[i].screen,
            dataFromDB[i].backCamera,
            dataFromDB[i].fontCamera,
            dataFromDB[i].img,
            dataFromDB[i].desc,
            dataFromDB[i].type,
        );
        ListProduct.push(mappedProduct);
    }
    return ListProduct;
}

const createListProduct = function (data) {
    let productHTML = "";
    for (let i = 0; i < data.length; i++) {
        productHTML += `
            <tr style="width: 100%;">
                <td>${ListProduct[i].id}</td>
                <td>${ListProduct[i].name}</td>
                <td>${ListProduct[i].price}</td>
                <td style="width: 100px">${ListProduct[i].screen}</td>
                <td>${ListProduct[i].backCamera}</td>
                <td>${ListProduct[i].fontCamera}</td>
                <td>
                    <img src="${ListProduct[i].img}" alt="img" style="width: 100px;">
                </td>
                 <td>${ListProduct[i].type}</td>
                <td>${ListProduct[i].desc}</td>
                <td style="width: 300px">
                    <button class="btn btn-success" onclick="handleGetInfo('${data[i].id}')">Sửa</button>
                    <button class="btn btn-danger" onclick="handleDeleteProduct('${data[i].id}')"> Xóa</button>
                </td >
            </tr >
    `;
    }
    getEle("tblDanhSachSanPham").innerHTML = productHTML;
}

const handleCreateProduct = function () {
    // DOM input
    const id = getEle("productID").value;
    const name = getEle("tenSanpham").value;
    const price = getEle("giaSanpham").value;
    const screen = getEle("manHinh").value;
    const backCamera = getEle("backcamera").value;
    const fontCamera = getEle("fontcamera").value;
    const type = getEle("loai").value;
    const img = getEle("hinhSanpham").value;
    const desc = getEle("moTa").value;

    const product = new Product(id, name, price, screen, backCamera, fontCamera, type, img, desc);

    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "POST",
        data: product,
    })
        .then(function (res) {
            console.log(res);
            createListProduct(data);
        })
        .catch(function (err) {
            console.log(err);
        })
}

const handleDeleteProduct = function (id) {
    console.log("ID:", id);
    axios({
        url: ` https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
        method: "DELETE",
    })
        .then((result) => {
            console.log(result);
            getListProduct();
        })
        .catch((err) => {
            console.log(err);
        })
}

const handleGetInfo = function (id) {
    console.log(id);
    axios({
        url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
        method: "GET"
    })
        .then((result) => {
            console.log(result);
            getEle("productID").value = result.data.id;
            getEle("tenSanpham").value = result.data.name;
            getEle("giaSanpham").value = result.data.price;
            getEle("manHinh").value = result.data.screen;
            getEle("backcamera").value = result.data.backCamera;
            getEle("fontcamera").value = result.data.fontCamera;
            loai = loai === "loai1" ? "iphone" : "samsung";
            getEle("loai").value = result.data.type;
            getEle("hinhSanpham").value = result.data.img;
            getEle("moTa").value = result.data.desc;
            getEle("productID").setAttribute("disabled", true);
            getEle("btnThem").style.display = "none"
        })
        .catch((err) => {
            console.log(err);
        })
}

const handleUpdate = function (id) {
    getEle("productID").setAttribute("disabled", true);
    const name = getEle("tenSanpham").value;
    const price = getEle("giaSanpham").value;
    const screen = getEle("manHinh").value;
    const backCamera = getEle("backcamera").value;
    const fontCamera = getEle("fontcamera").value;
    const type = getEle("loai").value;
    const img = getEle("hinhSanpham").value;
    const desc = getEle("moTa").value;

    const udateProduct = new Product(name, price, screen, backCamera, fontCamera, type, img, desc);
    axios({
        url: ` https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
        method: "PUT",
        data: udateProduct
    })
        .then((result) => {
            console.log(result);
            getListProduct();
        })
        .catch((err) => {
            console.log(err);
        })
}


getListProduct();
console.log("Danh sach Product", ListProduct);